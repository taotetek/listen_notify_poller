Listen / Notify Poller
======================

Synopsis
--------

This is an example of using PostgreSQL 9.0's "LISTEN / NOTIFY" functionality for asyncronous communication between two threads.  The example is written in JRuby, and uses JDBC for database communication.

Requirements
------------

1. Postgresql 9.0
2. Jruby

Background Material
-------------------

See the PostgreSQL 9.0 documentation on LISTEN / NOTIFY:

[Listen Documentation](http://www.postgresql.org/docs/9.0/static/sql-listen.html).

[Notify Documentation](http://www.postgresql.org/docs/9.0/static/sql-notify.html).

Why?
----

I wanted to learn how to use straight jdbc from jruby, and I wanted to play with the new "payload" functionality added to LISTEN / NOTIFY in 9.0

Setup
-----

    cd listen_notify_poller
    rake setup_example
    jruby ./example.rb

What It Does
------------

The setup task will create the database "listen_notify_poller" on your local PostgreSQL 9 instance, and run the following SQL:

    CREATE TABLE watched_table (
      id              SERIAL PRIMARY KEY,
      value           INT,
      date_updated    TIMESTAMP NOT NULL DEFAULT now()
    );

    CREATE FUNCTION notify_trigger() RETURNS trigger AS $$
    DECLARE

    BEGIN
      PERFORM pg_notify('watchers', TG_TABLE_NAME || ',id,' || NEW.id );
      RETURN new;
    END;
    $$ LANGUAGE plpgsql;

    CREATE TRIGGER watched_table_trigger AFTER INSERT ON watched_table FOR EACH ROW EXECUTE PROCEDURE notify_trigger()

Whenever a record is inserted into the "watched_table" table, the trigger will send a notification to the "watchers" channel that includes the table name, and the id of the new entry.

The "example.rb" script will do the following:

1. Create two threads, each with a connection to the listen_notify_poller
2. The insert thread inserts a record into watched_table every three seconds
3. The listen thread polls the notification queue every second, and when a notification is received of a new record, retrives it.

Example Output
--------------

    ~/src/rvm/listen_notify_poller$ jruby ./example.rb 
    polling...
    NOTIFICATION ------------
    pid: 6129
    name: watchers
    param: watched_table,id,1
    -------------------------
    RETRIEVING RECORD FOR NOTIFIED ID: 1
    id: 1
    value:  1
    record_time:  2010-10-03 11:17:17.710667
    -------------------------
    polling...
    polling...
    polling...
    NOTIFICATION ------------
    pid: 6129
    name: watchers
    param: watched_table,id,2
    -------------------------
    RETRIEVING RECORD FOR NOTIFIED ID: 2
    id: 2
    value:  1
    record_time:  2010-10-03 11:17:20.734844
    -------------------------
    polling...
    polling...
    polling...
    NOTIFICATION ------------
    pid: 6129
    name: watchers
    param: watched_table,id,3
    -------------------------
    RETRIEVING RECORD FOR NOTIFIED ID: 3
    id: 3
    value:  1
    record_time:  2010-10-03 11:17:23.817864
    -------------------------

